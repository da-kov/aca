
NAME = report
RM = $(NAME).dvi $(NAME).ps $(NAME).aux $(NAME).log $(NAME).toc $(NAME).out
ZIPFILE = xkovar66-fit

pdflatex:
	pdflatex $(NAME).tex


$(NAME).pdf: $(NAME).ps
	ps2pdf $(NAME).ps

$(NAME).ps: $(NAME).dvi
	dvips -t a4 $(NAME).dvi

$(NAME).dvi: $(NAME).tex
	latex $(NAME).tex
	latex $(NAME).tex

	
clean:
	rm -f $(RM)

pack: $(NAME).tex 
	zip $(ZIPFILE) $(NAME).tex Makefile

