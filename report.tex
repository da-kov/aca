\documentclass[a4paper, 12pt]{article}

\usepackage[left=1.5cm, top=2.5cm, text={18cm, 25cm}]{geometry}
\usepackage[utf8]{inputenc}

\usepackage{graphicx}   


\title{Advanced Computer Architectures}	
\author{David Kovarik\\ Fabio Verbosio}
%\date{May 17, 2017}	

\begin{document}
\maketitle

\section{Introduction}
The goal of this project is to optimize an architecture for a specific benchmark. The benchmark is focused on running encryption/decryption of texts using the AES algorithm. 

Architecture we are meant to optimize is called PISA and the optimization process goes in several steps. First we are going to select parameters, which will be subject to modification, then we are going to select, which attributes will be inspected. After that we run several different configuration to see, how each parameter influenced these attributes.

We will plot results of these configuration into a 2D space, so it is easier to select an optimal architecture(s).

\section{Benchmark description}
The benchmark consists of running the AES encryption/decryption over a set of the messages. AES is a modern algorithm for symmetric cryptography. The key size used in the benchmark is 32B (256b), which is encoded on the input as a 64 hexadecimal characters.

There are two pre-prepared input files in the benchmark. The smaller one consists of $311\,824$ characters. The longer input consists of $3\,247\,552$. We chose to work with \textbf{the smaller one}, because thanks to the characteristics of AES, the effort should remain proportionately same.

Obviously we can choose whether to perform encrypting, decrypting, or both. However, since both operations use the same operations, we decided to perform only encryption. The statistics should remain same for both.

\emph{\textbf{Note}: The benchmark source we downloaded from the given link contained errors and we weren't able to compile it. We had to fix the source first. Therefore we submit the benchmark along with the report.}

\section{Architectural parameters}
In this section we describe the parameters we used to change configuration of the default architecture. There are plenty of options to choose from, however thanks to the characteristics of the AES algorithm, there is no point in trying all of them. 

For example AES is designed to work in integer arithmetic, therefore there is no point in increasing number of floating point units. More FP units wouldn't increase performance, but would consume more energy.

\subsection{Analysis at the run time}

To analyze which parameters are suitable for experimenting we ran \texttt{simplescalar} profiling tool. We found out what instructions are executed the most. The most important instructions (groups) are shown in table \ref{tab:instRatio}.

\begin{table}[ht]
  \label{tab:instRatio}
\begin{center}
\begin{tabular}{ | c | c |}
  \hline
  Instruction (groups) & Occurrence [\%] \\ \hline\hline
  Branches & 5.2  \\\hline  
  Loads    & 26.4 \\\hline
  Stores   & 7.9 \\\hline
  Addition & 23.0 \\\hline
  Logical op. & 18.8 \\\hline
  Bit Shifts & 16.6 \\\hline
\end{tabular}
\end{center}
  \caption{Ratio of executed instruction groups.}
\end{table}


We can see that arithmetic operations along with load/store operation instructions are dominating the others. Therefore the main subjects for experimenting will be ALU units and caches.

Other instruction (or groups of instructions using the same hardware units) score lower than 5\%, therefore they are not part of the main space exploration. However can be subjects for experimenting later. Therefore we decided to take into consideration parameters from table \ref{tab:inspectedParams}.

\begin{table}[ht]
  \label{tab:inspectedParams}
\begin{center}
\begin{tabular}{ |c|c|l| }
  \hline
  \textbf{Parameter} & \textbf{Values} & \textbf{Description}\\\hline\hline

  bpred         & T, NT, bimod, 2lev, perfect, comb & Branch prediction method \\\hline
  bpred:bimod   & 128, 256, 512, 1024,\dots & bimodal predictor config\\\hline
  issue:width   & 1,2,4 & Number of instructions issued in a single cycle \\\hline
  issue:inorder & T, F & Run pipeline in-order/out-of-order \\\hline
  cache:dl1     & & L1 cache configuration \\\hline
  res:ialu      & 1,2,3,4 & Number of integer ALUs \\\hline
  res:imult     & 1,2,3,4 & Number of integer multipliers/dividers\\
  \hline
\end{tabular}
\end{center}
  \caption{Inspected parameters.}
\end{table}


After running simulation the main concern is what is the performance and the power consumption of the architecture. We use value of \texttt{sim\_cycle} to get information about the performance and value of \texttt{avg\_total\_power\_cycle} for information about power consumption. We use number of cycles as a metric for performance, because it is an obvious, easy to understand value and it can be easily compared. We use power unit per cycle as a measure for power consumption, because it is easier to comprare than consuption of the whole program run (it values could grow very large). 

\newpage
\section{Default configuration results}
The default configuration of the PISA architecture is already "powerful" enough. Therefore it was a good starting point for us to get an idea about the results we want to use. Table 3 shows the configuration (of parameters we changed in our simulations).

\begin{table}[ht]
  \label{tab:defaultConfig}
\begin{center}
\begin{tabular}{ |c|c| }
  \hline
  \textbf{Parameter} & \textbf{Values} \\\hline\hline
  bpred & bimod \\\hline
  bpred:bimod & 2048 \\\hline
  issue:width & 4 \\\hline
  issue:inorder & False \\\hline
  cache:dl1 & dl1:128:32:4:l\\\hline
  res:ialu & 4 \\\hline
  res:fplu & 4 \\\hline
  %\footnote{Configuration format of caches is: "name:nsets:bsize:assoc:repl"
\end{tabular}
  \caption{Configuration of the default architecture.}
\end{center}
\end{table}

\noindent The results retrieved from the simulation are shown in the table \ref{tab:defaultResults}.

\begin{table}[ht]
  \label{tab:defaultResults}
\begin{center}
\begin{tabular}{ |c|c| }
  \hline
  \textbf{Number of cycles} & \textbf{Energy per cycle} \\\hline\hline
  $19494226$& $71.7882$\\\hline
\end{tabular}
  \caption{Performance and power of the default architecture.}
\end{center}
\end{table}

From the configuration of this architecture, it is obvious that it is easy to find a different configuration, which dominates this one. The simple modification is in reducing number of floating point units to minimum (to 1, because less is not allowed).


\section{Experiments}
In this section we describe our approach of handling parameters across simulations. We want to rather describe correlations we discovered rather than actual space exploration (there is a dedicated section for that).

To explore the space of possible architectures we decided to go in a sequential way. Since integer and bitwise operations make up to 60\% of instructions, we first focused on number ALU units in the architecture. The second group of instructions (by number of executed instructions) are load/store instructions with up to 30\%. That's why the second step for us was to experiment with cache configuration. The last significant group with about 5\% were branch instruction -- therefore we focused on different branch prediction techniques. 

We took into consideration both aspects, performance and power consumed, and tried to modify parameters to get best results in both ways (high performance, but high power consumption, \dots), but also to find a good compromise.


\subsection{ALU units}
As said before, majority of instructions perform integer/bitwise operations, therefore it is the first concern to look into. Since AES is designed to consist only from integer arithmetic, there is no point in exploring influence of number of floating point units in the architecture. We just keep them in a minimum number, which is 1.

When adding number of ALU units we need to take into considerations the issue-width. There is no point of having more ALU units than the actual issue-width. Those extra units would just consume energy without being used properly.

By adding more ALU units we, obviously, increase performance. However these units increase energy consumption. Therefore we need to find a good trade off between these two measures.

If we want to increase performance, by only manipulating with ALU units, the principal is the more the better (up to the certain point). Adding a new ALU unit (within the same issue-width) increases power consumption by more or less constant values, while increase of performance goes with logarithmic decrease.

When we added up to 8 ALU units (issue width had to be increased to 8), we got even better performance. However the price (in form of energy cost) increased significantly (the increase is caused mostly by the issue-width increase). It is also important to say, that issue-width of 8 is basically unfeasible these days.

Therefore if we want to decrease power consumption as much as possible, we can go the other way around. Reducing number of ALU units to 1, as well as the issue width. We get a very bad performance, but low energy consumption.

\subsection{In-order/Out-of-Order execution}
While experimenting with the architecture, we found out, that enabling the out-of-order execution keeps the power consumption at the same level, but significantly increases the total number of cycles (performance). Therefore we decided that we are going to experiment with in-order execution only.

\subsection{Caches}
Another set of experiments focuses on cache optimization. We experimented with several cache sizes, number of sets, level of association.

As expected, the higher we make the cache, the more power consuming it will be, but performance can be improved. However power-wise it's the block size of the cache, which is more influential.

Experiments show, that increasing memory parameters above the default configuration doesn't bring significant amount of performance. However by decreasing them we can reduce power consumption quite well.

\subsection{Branch prediction}
The last group of instructions, with ratio of occurrence around 5\% are branching instruction. There are several branch predictors we can choose from -- \texttt{taken, nottaken, bimod, \dots} For \texttt{bimod} we can also choose the size of its table.

When using \texttt{bimodal} we have to define its table size. Using a larger table increases both performance and power consummation, its effect is rather small. Therefore it can be used for minor optimization (when slightly better performance need, but we don't want to spend too much energy).

Both \texttt{taken} and \texttt{not-taken} predictors provide very similar results. However both score lower in performance than \texttt{bimod}. Power consumption is more or less similar for all of the predictors. Therefore we came to conclusion, that branch predicting does not influence power consumption in a significant way.

\section{Space exploration}
In this section we provide visualization of several architectures from our experiments. Note that we removed some outliers, which were obviously dominated by other architectures, just to improve visual properties of the plot.

We also highlight points from the space lying on the pareto curve. These are architectures, which are not dominated by any other architecture (from our set).

In the figure 1 we can see the actual architecture distribution. On the y-axis there is performance (in number of cycles), and the power consumption on the x-axis. 


\begin{figure}[h!t]
  \label{fig:paramspace}
  \begin{center}
     \includegraphics[scale=0.5]{img/paramsSpace.pdf}
     \caption{Architectures in the explored space + pareto curve}
  \end{center}
 \end{figure}

 The way to interpret the distribution is that, for example, the architecture represented by the left-most point in the space is the most power-saving, but also the worst performance-wise. 

%\begin{table}
%  \label{tab:lowestPower}
% \begin{center}
%\begin{tabular}{ |c|c| }
%  \hline
%  \textbf{Parameter} & \textbf{Values} \\\hline\hline
%  bpred & bimod \\\hline
%  bpred:bimod & 2048 \\\hline
%  issue:width & 1 \\\hline
%  issue:inorder & False \\\hline
%  cache:dl1 & dl1:128:32:4:l\\\hline
%  res:ialu & 1 \\\hline
%  res:fplu & 1 \\\hline
%  %\footnote{Configuration format of caches is: "name:nsets:bsize:assoc:repl"
%\end{tabular}
%\end{center}
%  \caption{Architecture with the lowest power consumption.}
%\end{table}

% (26, 43, 44, 41, 40, 25, 21, 23, 31, 16, 19, 3, 28)

 We can see that we managed to discover several architecture with very similar performance, but very different power consumption. We can also see that there are two major clusters of architectures. Figures 2 and 3 proved closer look to them, respectively.

 \begin{figure}[h!t]
  \label{fig:cluster:1}
  \begin{center}
     \includegraphics[scale=0.5]{img/Cluster1.pdf}
     \caption{Detail of the left cluster}
  \end{center}
 \end{figure}

 \begin{figure}[h!t]
  \label{fig:cluster:2}
  \begin{center}
     \includegraphics[scale=0.5]{img/Cluster2.pdf}
     \caption{Detail of the right cluster}
  \end{center}
 \end{figure}

\newpage
\section{Conclusion}
Goal of this project was to perform a space exploration over variety of computer architectures, in two ways -- performance and power consumption, and to analyze influence of modifications of architectures on these two aspects.

The goal was to optimize an architecture for a specific benchmark, which, in our case, was AES encryption. We used the tool \emph{simplescalar} to discover which instructions are being executed the most. It confirmed out theory, that since AES is designed to work in integer arithmetic, the majority of instructions are arithmetic (integer). Load/store instruction are second followed by branching instructions. We focused on experimenting with these three groups.

Since the default PISA architecture is already "powerful", we used it as a starting point. We tried to improve this architecture in both direction -- performance, energy.

Since majority of instructions are arithmetic, number of ALU units in the architecture is crucial. By increasing it (taking issue-width into account) we can improve performance significantly (up to a certain point). On the other hand by removing ALUs we can save energy.

On step was to analyze influence of cache configuration. We found out, that enlarging the cache doesn't play that much important role. However there is some improvement in the performance. Similarly to the power.

At last we experimented with the branch prediction mechanism. We found out that all the mechanism provide similar results power-wise, but predictors \texttt{taken}, \texttt{not-taken} do worse in performance. Also that making table of \texttt{bimod} predictor smaller, doesn't make much of a difference.
\end{document}             % End of documentz